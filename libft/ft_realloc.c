/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 13:47:34 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/02 13:49:05 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *ptr, size_t size)
{
	void	*dst;

	if (!(dst = ft_memalloc(size)))
		return (NULL);
	if (ptr)
	{
		ft_memcpy(dst, ptr, size);
		ft_memdel(&ptr);
	}
	return (dst);
}
