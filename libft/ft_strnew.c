/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 00:01:10 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/03 21:41:07 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	size_t	i;
	char	*zone;

	i = 0;
	zone = malloc(sizeof(char) * (size + 1));
	if (zone == NULL)
		return (NULL);
	while (i < size)
	{
		zone[i] = '\0';
		i++;
	}
	zone[i] = '\0';
	return (zone);
}
