/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 09:59:39 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/06 00:42:00 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*s1;
	unsigned char	*s2;
	unsigned char	s3;

	i = 0;
	s1 = (unsigned char *)src;
	s2 = (unsigned char *)dst;
	s3 = (unsigned char)c;
	while (i < n)
	{
		s2[i] = s1[i];
		if (s1[i] == s3)
			return ((char *)&s2[i + 1]);
		i++;
	}
	return (NULL);
}
