/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:52:32 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/06 03:09:51 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		j;
	int		start;

	i = 0;
	j = 0;
	start = i;
	if (!s2[j])
		return ((char *)s1);
	while (s1[i] && s2[j])
	{
		if (s1[i] == s2[j])
		{
			if (s1[i - 1] != s2[j - 1])
				start = i;
			j++;
		}
		else
		{
			i = start++;
			j = 0;
		}
		i++;
	}
	return (!s2[j] && j != 0 ? (char *)&s1[start] : NULL);
}
