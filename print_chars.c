/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_chars.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 14:02:43 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/21 19:44:05 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				get_char(va_list av, char *flag)
{
	if (!flag)
		print_and_count(va_arg(av, int));
	else
		return (0);
	return (1);
}
