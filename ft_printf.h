/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 21:30:58 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/22 16:35:32 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft/libft.h"
# include <stdarg.h>
# include <stdio.h>
# include <wchar.h>

int				g_count;
int				(*g_tab[52])(va_list av, char *flag);

int				ft_printf(const char *str, ...);
int				starts_with(char *src, char *str);
void			print_and_count(char c);
// void			print_wchar_and_count(wchar_t c);
int				woods_of_if(char *str, va_list av);
int				get_whole_number(va_list av, char *flag);
int				get_decimal_number(va_list av, char *flag);
int				get_string(va_list av, char *flag);
int				get_char(va_list av, char *flag);

#endif
