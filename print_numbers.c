/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_numbers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 11:19:51 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/21 19:44:47 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		print_whole_number(__int128 n)
{
	if (n < 0 && (n = -n))
		print_and_count('-');
	if (n >= 10)
	{
		print_whole_number(n / 10);
		print_whole_number(n % 10);
	}
	else
		print_and_count('0' + n);
}

int				get_whole_number(va_list av, char *flag)
{
	if (!flag)
		print_whole_number((__int128)va_arg(av, int));
	else if (!ft_strcmp(flag, "l"))
		print_whole_number((__int128)va_arg(av, long));
	else if (!ft_strcmp(flag, "ll"))
		print_whole_number((__int128)va_arg(av, long long));
	else
		return (0);
	return (1);
}

static void		print_decimal_number(long double n)
{
	(void)n;
}

int				get_decimal_number(va_list av, char *flag)
{
	if (!flag)
		print_decimal_number((long double)va_arg(av, double));
	else
		return (0);
	return (1);
}
