/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   woods_of_if.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 11:47:42 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/31 13:54:30 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int			check_type(char s, va_list av, char *flag)
{
	int				c;

	if (((s > 64 && s < 91) || (s > 96 && s < 123)) &&
		g_tab[(c = s > 90 ? s - 71 : s - 65)])
		return (g_tab[c](av, flag));
	return (-1);
}

int					woods_of_if(char *str, va_list av)
{
	static char		*flags[6] = {"hh", "ll", "h", "l", "j", "z"};
	unsigned long	i;

	i = 0;
	if (check_type(str[0], av, NULL) < 0)
	{
		while (i < 6 && !starts_with(str, flags[i]))
			i++;
		if (i < 6 && check_type(str[ft_strlen(flags[i])], av, flags[i]) == 1)
			return ((int)ft_strlen(flags[i]));
	}
	return (0);
}
