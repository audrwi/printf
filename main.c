/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 13:41:43 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/22 16:59:39 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <locale.h>

int			main(void)
{
	setlocale(LC_ALL, "en_US.UTF-8");
	printf("%d\n", printf("%s %ls %d ok\n", "Щésζ", L"Щésζ", 4294967));
	ft_printf("%d\n", ft_printf("%s %ls %d ok\n", "Щésζ", L"Щésζ", 4294967));
	// printf("%d\n", printf("%f, %f, %.6f, %1.6f, %f, %.8f\n", 2.2, 2.2f, 2.2, 2.2f, 2.333333333333, 2.555555555555555));
	// ft_printf("%d\n", ft_printf("%f, %f, %.6f, %1.6f, %f, %.8f\n", 2.2, 2.2f, 2.2, 2.2f, 2.333333333333, 2.555555555555555));
	// unsigned char c;
	// c = 0xe1;
	// write(1, &c, 1);
	// c = 0x88;
	// write(1, &c, 1);
	// c = 0xb4;
	// write(1, &c, 1);
	// c = 0x0a;
	// write(1, &c, 1);
	// return (0);
}
