# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/03/10 18:20:16 by aboucher          #+#    #+#              #
#    Updated: 2017/03/21 17:24:08 by aboucher         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC = libft/ft_memset.c		\
	  libft/ft_bzero.c		\
	  libft/ft_memcpy.c		\
	  libft/ft_memccpy.c	\
	  libft/ft_memmove.c	\
	  libft/ft_memchr.c		\
	  libft/ft_memcmp.c		\
	  libft/ft_strlen.c		\
	  libft/ft_strdup.c		\
	  libft/ft_strcpy.c		\
	  libft/ft_strncpy.c	\
	  libft/ft_strcat.c		\
	  libft/ft_strncat.c	\
	  libft/ft_strlcat.c	\
	  libft/ft_strchr.c		\
	  libft/ft_strrchr.c	\
	  libft/ft_strstr.c		\
	  libft/ft_strnstr.c	\
	  libft/ft_strcmp.c		\
	  libft/ft_strncmp.c	\
	  libft/ft_atoi.c		\
	  libft/ft_isalpha.c	\
	  libft/ft_isdigit.c	\
	  libft/ft_isalnum.c	\
	  libft/ft_isascii.c	\
	  libft/ft_isprint.c	\
	  libft/ft_toupper.c	\
	  libft/ft_tolower.c	\
	  libft/ft_memalloc.c	\
	  libft/ft_memdel.c		\
	  libft/ft_strnew.c		\
	  libft/ft_strdel.c		\
	  libft/ft_strclr.c		\
	  libft/ft_striter.c	\
	  libft/ft_striteri.c	\
	  libft/ft_strmap.c		\
	  libft/ft_strmapi.c	\
	  libft/ft_strequ.c		\
	  libft/ft_strnequ.c	\
	  libft/ft_strsub.c		\
	  libft/ft_strjoin.c	\
	  libft/ft_strtrim.c	\
	  libft/ft_strsplit.c	\
	  libft/ft_itoa.c		\
	  libft/ft_putchar.c	\
	  libft/ft_putstr.c		\
	  libft/ft_putendl.c	\
	  libft/ft_putnbr.c		\
	  libft/ft_putchar_fd.c	\
	  libft/ft_putstr_fd.c	\
	  libft/ft_putendl_fd.c	\
	  libft/ft_putnbr_fd.c	\
	  libft/ft_lstnew.c		\
	  libft/ft_lstdelone.c	\
	  libft/ft_lstdel.c		\
	  libft/ft_lstadd.c		\
	  libft/ft_lstiter.c	\
	  libft/ft_lstmap.c		\
	  libft/ft_strtrimc.c	\
	  libft/ft_strndup.c	\
	  libft/ft_sqr.c		\
	  libft/ft_sqrt.c		\
	  libft/ft_realloc.c	\
	  libft/ft_strjoinf.c	\
	  libft/get_next_line.c	\
	  libft/ft_isblank.c	\
	  libft/ft_abs.c		\
	  libft/ft_strtolower.c	\
	  libft/ft_puterror.c	\
	  ft_printf.c			\
	  functions.c			\
	  woods_of_if.c			\
	  print_numbers.c		\
	  print_strings.c		\
	  print_chars.c

OBJ = $(SRC:.c=.o)

HEAD = ft_printf.h

FLAGS = -Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "\rCreating Library...	\033[0;32m[OK]\033[0m"
	@echo "\033[0;36m"
	@echo "██████╗ ██████╗ ██╗███╗   ██╗████████╗███████╗"
	@echo "██╔══██╗██╔══██╗██║████╗  ██║╚══██╔══╝██╔════╝"
	@echo "██████╔╝██████╔╝██║██╔██╗ ██║   ██║   █████╗  "
	@echo "██╔═══╝ ██╔══██╗██║██║╚██╗██║   ██║   ██╔══╝  "
	@echo "██║     ██║  ██║██║██║ ╚████║   ██║   ██║     "
	@echo "╚═╝     ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝   ╚═╝   ╚═╝     "
	@echo "\033[0m"

%.o: %.c $(HEAD)
	@echo "\rCreating Library...	\c"
	@gcc $(FLAGS) -o $@ -c $< -I$(HEAD)

clean:
	@echo "Cleaning objects...	\c"
	@rm -f $(OBJ)
	@echo "\rCleaning objects...	\033[0;32m[OK]\033[0m"

fclean: clean
	@echo "Cleaning target...	\c"
	@rm -f $(NAME)
	@echo "\rCleaning target...	\033[0;32m[OK]\033[0m"

re: fclean all

.PHONY: all clean fclean re
