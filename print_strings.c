/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_strings.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 13:42:34 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/22 16:35:42 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void			print_string(char *str)
{
	size_t			i;

	i = 0;
	while (i < ft_strlen(str))
	{
		print_and_count(str[i]);
		i++;
	}
}

static void			print_wchar(wchar_t *src)
{
	unsigned long	i;

	i = 0;
	while (i < sizeof(src) - 1)
	{
		print_and_count(src[i]);
		i++;
	}
}

int					get_string(va_list av, char *flag)
{
	if (!flag)
		print_string(va_arg(av, char *));
	else if (!ft_strcmp(flag, "l"))
		print_wchar(va_arg(av, wchar_t *));
	else
		return (0);
	return (1);
}
