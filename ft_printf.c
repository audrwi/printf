/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 14:46:07 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/21 19:43:52 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		init_globals(void)
{
	g_count = 0;
	g_tab[28] = get_char;
	g_tab[29] = get_whole_number;
	g_tab[31] = get_decimal_number;
	g_tab[34] = get_whole_number;
	g_tab[44] = get_string;
}

int				ft_printf(const char *str, ...)
{
	va_list		av;

	init_globals();
	va_start(av, str);
	while (*str)
	{
		if (*str == '%' && str++)
			str += woods_of_if((char *)&(*str), av);
		else
			print_and_count(*str);
		str++;
	}
	va_end(av);
	return (g_count);
}
