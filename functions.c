/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/30 15:06:13 by aboucher          #+#    #+#             */
/*   Updated: 2017/03/24 14:22:28 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				starts_with(char *src, char *str)
{
	int			i;

	i = 0;
	if (!src || !str)
		return (0);
	while (str[i])
	{
		if (src[i] != str[i])
			return (0);
		i++;
	}
	return (1);
}

void			print_and_count(char c)
{
	int			i;
	static char	bits[9];

	i = 8;
	while (--i >= 0)
		bits[i] = (c & (1 << i)) ? '1' : '0';
	bits[8] = '\0';
	ft_putchar(c);
	ft_putendl(bits);
	// ft_putchar(c);
	g_count++;
}
